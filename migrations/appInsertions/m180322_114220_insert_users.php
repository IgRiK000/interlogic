<?php

use yii\db\Migration;

/**
 * Class m180322_100253_insert_admin_user
 */
class m180322_114220_insert_users extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('{{%user}}', [
            'username' => 'admin',
            'email' => 'admin@text.ua',
            'auth_key' => \Yii::$app->security->generateRandomString(),
            'password_hash' => \Yii::$app->security->generatePasswordHash('admin'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%user}}', [
            'username' => 'demo',
            'email' => 'demo@text.ua',
            'auth_key' => \Yii::$app->security->generateRandomString(),
            'password_hash' => \Yii::$app->security->generatePasswordHash('demo'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%auth_assignment}}', [
            'item_name' => 'admin',
            'user_id' => \app\models\User::findByUsername('admin')->getId(),
        ]);
    }

    public function down()
    {
        $this->delete('{{%auth_assignment}}', ['user_id' => \app\models\User::findByUsername('admin')->getId()]);
        $this->delete('{{%user}}', ['username' => 'admin']);
        $this->delete('{{%user}}', ['username' => 'demo']);
    }

}

<?php

use yii\db\Migration;

/**
 * Class m180322_114220_insert_roles
 */
class m180322_100253_insert_roles extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'admin',
        ]);
    }

    public function down()
    {
        echo "m180322_114220_insert_roles cannot be reverted.\n";

        return false;
    }

}
